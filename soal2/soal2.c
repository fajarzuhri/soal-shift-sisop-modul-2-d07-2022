#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

typedef struct {
	char name[25];
	char genre[10];
	int year;
} drakor;

int comparator(const void *v1, const void *v2) {
	drakor *p1 = (drakor *) v1;
	drakor *p2 = (drakor *) v2;
	if (p2->year == 0) return -1;
	if (strcmp(p1->genre, p2->genre)) return strcmp(p1->genre, p2->genre);
	if (p1->year < p2->year) return -1;
	if (p1->year > p2->year) return 1;
	return 0;
}

void remove_file_recursively(char *basePath) {
	char path[1000];
	struct dirent *dp;
	DIR *dir = opendir(basePath);

	if (!dir) return;

	while((dp = readdir(dir)) != NULL) {
		if(strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")) {
			strcpy(path, basePath);
			strcat(path, "/");
			strcat(path, dp->d_name);

			remove_file_recursively(path);
			remove(path);
		}
	}
	remove(basePath);
}

drakor l[25];

int main() {

	pid_t child_id;
	int status;
	DIR *dp;
	struct dirent *ep;

	int i = 0;


	child_id = fork();

	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if (child_id == 0) {
		execl("/bin/mkdir", "mkdir", "-p", "/home/fajarzuhri/shift2/drakor", NULL);
	} else {
		while ((wait(&status)) > 0);
		pid_t child_id2 = fork();
		int status2;

		if (child_id2 == 0) {
			execl("/bin/unzip", "unzip", "-q", "drakor.zip", "-d", "/home/fajarzuhri/shift2/drakor", NULL);

		} else {
			while((wait(&status2)) > 0);
			if ((chdir("/home/fajarzuhri/shift2/drakor")) < 0) {
				puts("ERROR in CHDIR");
				exit(EXIT_FAILURE);
			}

			dp = opendir(".");
			if (dp != NULL) {
				while ((ep = readdir(dp))) {
					if (strcmp(ep->d_name, ".") && strcmp(ep->d_name, "..")) {
						char *dot = strrchr(ep->d_name, '.');
						if (dot == NULL) {
							char path[1000] = "/home/fajarzuhri/shift2/drakor/";
							strcat(path, ep->d_name);
							remove_file_recursively(path);
						} else if (dot && (strcmp(dot, ".png")) == 0) {
							char file[strlen(ep->d_name)];
							strcpy(file, ep->d_name);

							char *filename = strtok(file, ".");
							char filename2[strlen(ep->d_name)];
							strcpy(filename2, filename);

							char *end1, *end2;
							char *movie = strtok_r(filename2, "_", &end1);

							while(movie != NULL) {
								char movie2[strlen(movie)];
								strcpy(movie2, movie);

								char *movie_name = strtok_r(movie2, ";", &end2), *year, *genre;
								year = strtok_r(NULL, ";", &end2);
								genre = strtok_r(NULL, ";", &end2);

								pid_t child_id_cp = fork();
								int status_cp;

								if (child_id_cp == 0) {
									execl("/bin/mkdir", "mkdir", "-p", genre, NULL);
								} else {
									while((wait(&status_cp)) > 0);
									char *dest;
									strcpy(dest, genre);
									strcat(dest, "/");
									strcat(dest, movie_name);
									strcat(dest, ".png");
//									puts(dest);
									movie = strtok_r(NULL, "_", &end1);

//									drakor *movie_str;
//									movie_str = malloc(sizeof(drakor));
//									strcpy(movie_str->name, movie_name);
//									strcpy(movie_str->genre, genre);
//									movie_str->year = atoi(year);
//									list_movie[i++] = movie_str;
									strcpy(l[i].name, movie_name);
									strcpy(l[i].genre, genre);
									l[i++].year = atoi(year);
									if (0 == fork()) {
										continue;
									}
									execl("/bin/cp", "cp", ep->d_name, dest, NULL);
								}
							}
							remove(ep->d_name);
						} else {
							remove(ep->d_name);
						}
					}
				}
				closedir(dp);
			} else {
				puts("ERROR in OPENDIR");
				exit(EXIT_FAILURE);
			}

			qsort(l, 25, sizeof(drakor), comparator);

			for (int i = 0; i < 25; i++) {
//				puts("HAA");
				if (strcmp(l[i].name, "")) {
					char filepath[100];
					strcpy(filepath, l[i].genre);
					strcat(filepath, "/data.txt");
					FILE *file;
					if(access(filepath, F_OK) != 0) {
						file = fopen(filepath, "w");
						fprintf(file, "kategori : %s\n\n", l[i].genre);
						fprintf(file, "nama : %s\n", l[i].name);
						fprintf(file, "rilis : tahun %d\n\n", l[i].year);
						fclose(file);
					} else {
						file = fopen(filepath, "a");
						fprintf(file, "nama : %s\n", l[i].name);
						fprintf(file, "rilis : tahun %d\n\n", l[i].year);
						fclose(file);
					}
//					printf("%s %s %d %s\n", l[i].name, l[i].genre, l[i].year, filepath);
//					puts("LOO");
				}
			}
		}

	}

	return 0;
}
