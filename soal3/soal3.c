#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>

int main(){
  pid_t cid1, cid2, cid3, cid4, cid5, cid6, cid7, cid8, cid9, cid10, cid11, cid12;
  int status;
  
  
///////////////////////////////////////////////////////////////////////////////////


  cid1 = fork(); // digunakan untuk membuat child proses pertama
  if(cid1 < 0){
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if(cid1 == 0){
    char *argv[] = {"mkdir", "-p", "/home/hadi/modul2/darat", NULL};
    execv("/bin/mkdir", argv); // digunakan untuk membuat folder darat
  } else{
    while((wait(&status)) > 0);

    
///////////////////////////////////////////////////////////////////////////////////


    cid2 = fork(); // digunakan untuk membuat child proses kedua
    if(cid2 < 0){
      exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(cid2 == 0){
      sleep(3); // digunakan untuk membuat proses terhenti selama 3 detik
      char *argv[] = {"mkdir", "-p", "/home/hadi/modul2/air", NULL};
      execv("/bin/mkdir", argv); // digunakan untuk membuat folder air
    } else{
      while((wait(&status)) > 0);
      

////////////////////////////////////////////////////////////////////////////////////

    
      cid3 = fork(); // digunakan untuk membuat child proses ketiga
      if(cid3 < 0){
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
      }

      if(cid3 == 0){
        char *argv[] = {"unzip", "-q", "animal.zip", NULL};
        execv("/usr/bin/unzip", argv); // digunakan untuk mrngunzip file animal.zip
      } else{
        while((wait(&status)) > 0);

        
/////////////////////////////////////////////////////////////////////////////////////


      cid4 = fork(); // digunakan untuk membuat child proses keempat
     if(cid4 < 0){
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
 	}        
     if(cid4 == 0){ 
        char *argv[]={"find", "/home/hadi/modul2/animal", "-type", "f", "-name", "*darat.jpg", "-exec", "mv", "-t", "/home/hadi/modul2/darat", "{}", "+", (char *) NULL};
        execv("/usr/bin/find",argv); // digunakan supaya file hewan darat di folder animal bisa masuk ke folder darat
        } else{
        while((wait(&status)) > 0);


//////////////////////////////////////////////////////////////////////////////////////


      cid5 = fork(); // digunakan untuk membuat child proses kelima
      if(cid5 < 0){
        exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
      if(cid5 == 0){
        sleep(3); // digunakan untuk membuat proses terhenti selama 3 detik
        char *argv[]={"find", "/home/hadi/modul2/animal", "-type", "f", "-name", "*air.jpg", "-exec", "mv", "-t", "/home/hadi/modul2/air", "{}", "+", (char *) NULL};
        execv("/usr/bin/find",argv); // digunakan supaya file hewan air di folder animal bisa masuk ke folder air
        } else{
        while((wait(&status)) > 0);
        

////////////////////////////////////////////////////////////////////////////////////////


       cid6 = fork(); // digunakan untuk membuat child proses keenam
       if(cid6 < 0){
         exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
         }
      if(cid6 == 0){
        char *argv[]={"find", "/home/hadi/modul2/animal", "-type", "f", "-name", "*fish.jpg", "-exec", "mv", "-t", "/home/hadi/modul2/air", "{}", "+", (char *) NULL};
        execv("/usr/bin/find",argv); // digunakan supaya file hewan air yang tertinggal di folder animal masuk ke folder air
        } else{
        while((wait(&status)) > 0);


///////////////////////////////////////////////////////////////////////////////////////////


       cid7 = fork(); // digunakan untuk membuat child proses ketujuh
       if(cid7 < 0){
         exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
         }
       if(cid7 == 0){
         char *arg[]={"find", "/home/hadi/modul2/animal", "-type", "f", "-name", "*bird.jpg", "-exec", "mv", "-t", "/home/hadi/modul2/darat", "{}", "+", (char *) NULL};
         execv("/usr/bin/find",arg); // digunakan supaya file hewan burung di folder animal bisa masuk di folder darat
         } else{
         while((wait(&status)) > 0);
         

///////////////////////////////////////////////////////////////////////////////////////////



       cid8 = fork(); // digunakan untuk membuat child proses kedelapan
       if(cid8 < 0){
         exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
         }
       if(cid8 == 0){
         char *arg[]={"find", "/home/hadi/modul2/animal", "-type", "f", "-name", "*frog.jpg", "-exec", "rm", "-r", "{}", "+", (char *) NULL};
         execv("/usr/bin/find",arg); // digunakan untuk menghapus isi dari folder animal
         } else{
         while((wait(&status)) > 0);
         

//////////////////////////////////////////////////////////////////////////////////////////////



       cid9 = fork(); // digunakan untuk membuat child proses kesembilan
       if(cid9 < 0){
         exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
         }
       if(cid9 == 0){
         char *arg[]={"find", "/home/hadi/modul2/darat", "-type", "f", "-name", "*bird.jpg", "-exec", "rm", "-r",  "{}", "+", (char *) NULL};
         execv("/usr/bin/find",arg); // digunakan untuk menghapus file burung
         } else{
         while((wait(&status)) > 0);
         

///////////////////////////////////////////////////////////////////////////////////////////////



        cid10 = fork(); // digunakan untuk membuat child proses kesepuluh
        if(cid10 < 0){
          exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
          } 
        if(cid10 == 0){
          char *arg[]={"find", "/home/hadi/modul2/darat", "-type", "f", "-name", "*bird_darat.jpg", "-exec", "rm", "-r", "{}", "+", (char *) NULL};
          execv("/usr/bin/find",arg); // digunakan untuk menghapus seluruh file burung yang tertinggal
          } else{
          while((wait(&status)) > 0);


//////////////////////////////////////////////////////////////////////////////////////////////////


        cid11 = fork();
        if(cid11 < 0){
          exit(EXIT_FAILURE);
          }
        if(cid11 == 0){
          char *argv[] = {"touch", "/home/hadi/modul2/air/list.txt", NULL};
          execv("/bin/touch", argv); // digunakan untuk membuat folder darat
          } else{
          while((wait(&status)) > 0);
          

///////////////////////////////////////////////////////////////////////////////////////////////////


        cid12 = fork();
        if(cid12 < 0){
          exit(EXIT_FAILURE);
          }
    
}
}  
}         
}
}
}
}
}
}
}
}
}
