# soal-shift-sisop-modul-2-D07-2022

Praktikum 2 Sistem Operasi 2022

## Anggota Kelompok
- Fitra Agung Diassyah Putra - 5025201072
- Ananda Hadi Saputra - 5025201148
- Fajar Zuhri Hadiyanto - 5025201248

## Soal 1

### Eksekusi File
Eksekusi File dilakukan dengan cara "gcc soal1.c -ljson-c -o soal1" dan "./soal1". diperlukan -ljson-c karena pada soal ini memakai library json 
![Gambar 15](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal1/1648382258304.jpg)

Setelah file soal1.c dicompile menjadi soal1 dan soal1 dieksekusi, maka file character.zip dan weapon.zip akan terdownload dan akan dilakukan pembuatan folder gacha_gacha dengan isi characters dan weapons seperti gambar di bawah ini.
![Gambar 1](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal1/13471.jpg)
![Gambar 2](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal1/13472.jpg)

untuk mempermudah pengambilan nama file dimasukkan ke dalam file txt dengan isi semua nama file di folder character dan weapons, seperti pada gambar di bawah ini.
![Gambar 3](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal1/13474.jpg)
![Gambar 4](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal1/13475.jpg)

Sesuai dengan syarat dimana
syarat pertama : salah satu file character diambil saat jumlah gacha/ yang di program yang saya buat bernamakan pity berangka ganjil dan untuk weapon berangka genap 
syrarat kedua : program akan berjalan jika primogems masih diatas 160 dan cost setiap kali gacha adalah 160 primogems.
sebagai bentuk kasar program ditampilkan seperti gambar di bawah dengan format pity_tipe_sisa primogems
![Gambar 5](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal1/1648372395218.jpg)

### Kendala Yang DiHadapi
Kendala yang dihadapi adalah pembuatan directory yang masih belum terpenuhi dan print isi file json yang masih tidak bekerja. saat perintah untuk menmasukkan nama dan rarity dari item maka while cuma akan berjalan sekali dan tidak sampai primogems habis. dan tingkat kesusahan soal sangat susah untuk dikerjakan.

## Soal 2
### Eksekusi Script
![Gambar 6](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal-2/Screenshot+2022-03-27+142750+2.png)

Gambar diatas menunjukkan file soal2.c yang sudah dicompile menjadi file soal2 untuk kemudian dieksekusi. Setelah dieksekusi, maka file drakor.zip akan diekstrak ke dalam folder /home/fajarzuhri/shift2/drakor, lalu membuang semua file dan folder yang tidak relevan dengan poster film, lalu mengkategorisasikan film tersebut ke dalam masing-masing folder sesuai dengan genre film tersebut. Setelah itu, di masing - masing folder tersebut terdapat file data.txt yang berisi informasi genre dan list filmnya (nama dan tahun), yang diurutkan berdasarkan tahun rilis. Dari gambar di atas juga dapat dilihat terdapat 7 genre film berbeda, yaitu action, comedy, fantasy, horror, romance, school, dan thriller.

### Isi Folder Action
![Gambar 7](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal-2/Screenshot+2022-03-27+142750+3.png)

Gambar di atas menunjukkan isi dari folder action, dimana terdapat 4 buah poster film dan 1 buah file data.txt dengan isi seperti pada gambar.

### Isi Folder Comedy
![Gambar 8](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal-2/Screenshot+2022-03-27+142833.png)

Gambar di atas menunjukkan isi dari folder comedy, dimana terdapat 2 buah poster film dan 1 buah file data.txt dengan isi seperti pada gambar.

### Isi Folder Fantasy
![Gambar 9](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal-2/Screenshot+2022-03-27+143042.png)

Gambar di atas menunjukkan isi dari folder fantasy, dimana terdapat 2 buah poster film dan 1 buah file data.txt dengan isi seperti pada gambar.

### Isi Folder Horror
![Gambar 10](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal-2/Screenshot+2022-03-27+143114.png)

Gambar di atas menunjukkan isi dari folder horror, dimana terdapat 2 buah poster film dan 1 buah file data.txt dengan isi seperti pada gambar.

### Isi Folder Romance
![Gambar 11](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal-2/Screenshot+2022-03-27+143206.png)

Gambar di atas menunjukkan isi dari folder romace, dimana terdapat 4 buah poster film dan 1 buah file data.txt dengan isi seperti pada gambar.

### Isi Folder School
![Gambar 12](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal-2/Screenshot+2022-03-27+143232.png)

Gambar di atas menunjukkan isi dari folder school, dimana terdapat 3 buah poster film dan 1 buah file data.txt dengan isi seperti pada gambar.

### Isi Folder Thriller
![Gambar 13](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal-2/Screenshot+2022-03-27+143303.png)

Gambar di atas menunjukkan isi dari folder thriller, dimana terdapat 2 buah poster film dan 1 buah file data.txt dengan isi seperti pada gambar.

### Kendala Pengerjaan
1. Linux pada VirtualBox yang tiba-tiba crash setelah update windows ke build terbaru, alhasil harus restore windows sebelum update.
2. Saya coba di WSL tapi hasilnya berbeda dengan ketika dicoba di ubuntu VirtualBox.

## Soal 3

### Pengerjaan
1. masukan file yang dibutuhkan kedalam folder yang telah dibuat
2. jalan kan script dengan "gcc soal3.c -o soal3" dan "./soal3"
3. list seluruh hasil yang didapatkan

### Hasil
![Gambar 14](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal3/1.2.png)
![Gambar 14](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal3/1.1.png)
![Gambar 14](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-2/soal3/1.3.png)

### Kendala
1. soal yang cukup rumit
2. sulitnya mencari referensi yang ada di internet


