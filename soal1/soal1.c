
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <json-c/json.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/stat.h>
#include <dirent.h>
				
char characters[48][101];
char weapons[130][101];
int primogems = 79000;
int pity = 0;
char str_time[20];

//######################## REVISI ###########################

// void delay(int number_of_seconds)
// {
    
//     int milli_seconds = 1000 * number_of_seconds;
  
    
//     clock_t start_time = clock();
  
    
//     while (clock() < start_time + milli_seconds);
// }

int main() {
  
	time_t timenow;
	struct tm *timeinfo;
	time( &timenow );
	timeinfo = localtime( &timenow );

	pid_t child_id2;
	int status2;

	child_id2 = fork();

	if (child_id2 < 0) {
		exit(EXIT_FAILURE);
	}

	if (child_id2 == 0) {
		execl("/bin/mkdir", "mkdir", "-p", "gacha_gacha", NULL);
	}
	else {
		while ((wait(&status2)) > 0);
		pid_t child_id3;
		child_id3 = fork();
		int status3;

		if (child_id3 < 0) {
			exit(EXIT_FAILURE);
		}
		if (child_id3 == 0) {
			execl("/usr/bin/wget", "wget","-O","./characters.zip","https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","-q",NULL);
			//execl("/usr/bin/unzip", "unzip", "-q", "weapon.zip", "-d", "/home/fitra/SISOP/modul2/gacha_gacha", NULL);
		}
		else {
			while((wait(&status3)) > 0);
			pid_t child_id4;
			child_id4 = fork();
			int status4;

			if (child_id4 < 0) {
				exit(EXIT_FAILURE);
			}

			if(child_id4 == 0) {
				//execl("/usr/bin/unzip", "unzip", "-q", "characters.zip", "-d", "/home/fitra/SISOP/modul2/gacha_gacha", NULL);
				execl("/usr/bin/wget", "wget","-O","./weapons.zip","https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT","-q",NULL);;
			}
			else{
				while((wait(&status4) > 0));
				pid_t child_id5;
				child_id5 = fork();
				int status5;

				if (child_id5 < 0) {
					exit(EXIT_FAILURE);
				}
				if(child_id5 == 0) {
					//execl("/usr/bin/wget", "wget", "-O", "./characters.zip", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-q", NULL);
					execl("/usr/bin/unzip", "unzip", "-q", "characters.zip", "-d", "/home/fitra/SISOP/modul2/gacha_gacha" , NULL);
				}
				else{
					while((wait(&status5) > 0));
					pid_t child_id6;
					child_id6 = fork();
					int status6;

					if (child_id6 < 0) {
						exit(EXIT_FAILURE);
					}
					if(child_id6 == 0) {
						//execl("/usr/bin/wget","wget", "-O", "./weapon", " https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT  ", NULL);
						execl("/usr/bin/unzip", "unzip", "-q", "weapons.zip", "-d", "/home/fitra/SISOP/modul2/gacha_gacha", NULL);
					}
					else{
						while((wait(&status6) > 0));
						pid_t child_id7;
						child_id7 = fork();
						int status7;

						if (child_id7 < 0) {
						exit(EXIT_FAILURE);
						}
						if(child_id7 == 0) {
							
								DIR *dp; 
								struct dirent *entry;
								char path[100];
								FILE *fp;
								
								//insert file for characters
								fp=fopen("./gacha_gacha/characters/characters.txt","w");
								strcpy(path,"./gacha_gacha/characters");
								dp = opendir(path);

								while ((entry = readdir (dp))) {
									if(strlen(entry->d_name)>2) 
										fprintf(fp,"%s\n",entry->d_name);
								}closedir (dp);
								
								fclose(fp);

								//insert file for weapon
								fp=fopen("./gacha_gacha/weapons/weapons.txt","w");
								strcpy(path,"./gacha_gacha/weapons");
								dp = opendir(path);

								while ((entry = readdir (dp))) {
									if(strlen(entry->d_name)>2) 
										fprintf(fp,"%s\n",entry->d_name);
								}closedir (dp);
								
								fclose(fp);

								fp = fopen("./gacha_gacha/characters/characters.txt","r");	
								int i = 0; 
								while(1){ 
									char read = (char)fgetc(fp); 
									int j = 0; 
									while(read!='\n' && !feof(fp)){
										characters[i][j++] = read;			
										read = (char)fgetc(fp); 
										//printf("%d\t %s\n",i,characters[i]);
									} 
									characters[i][j]=0;		
									if(feof(fp)){		 
										break; 
									} 
									i++; 
								} 
								fclose(fp);

								fp = fopen("./gacha_gacha/weapons/weapons.txt","r");	
								i = 0; 
								while(1){ 
									char read = (char)fgetc(fp); 
									int j = 0; 
									while(read!='\n' && !feof(fp)){
										weapons[i][j++] = read;			
										read = (char)fgetc(fp); 
									} 
									weapons[i][j]=0;		
									if(feof(fp)){		 
										break; 
									} 
									i++; 
								} 
								fclose(fp);

								fp=fopen("./gacha_gacha/test.txt","w");
									strcpy(path,"./gacha_gacha");
									dp = opendir(path);

								while(primogems > 160){
									
									// sleep(1);

									char buffer[8192];
									struct json_object *parsed_json;
									struct json_object *name;
									struct json_object *rarity;
									char item[101];
									sprintf(str_time, "%02d:%02d:%02d\n", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);

									pity++;
									primogems = primogems - 160;

									//for print character
									if(pity%2 == 0){

										// strcpy(item, "gacha_gacha/characters/");
										// strcat(item,characters[rand()%48]);

										// fp = fopen(item,"r");
										// fread(buffer, 16384, 1, fp);
										// fclose(fp);

										parsed_json = json_tokener_parse(buffer);

										json_object_object_get_ex(parsed_json, "name", &name);
										json_object_object_get_ex(parsed_json, "rarity", &rarity);
										

										fprintf(fp,"%d_Characters_%d\n", pity, primogems);
										//  printf("%d_Characters_%d_%s_%d\n", pity, json_object_get_int(rarity), json_object_get_string(name), primogems);
										//printf("%d_Characters_%d\n", pity, primogems);

									}

									if(pity%2 == 1){
										
										// parsed_json = json_tokener_parse(buffer);

										// json_object_object_get_ex(parsed_json, "name", &name);
										// json_object_object_get_ex(parsed_json, "rarity", &rarity);
										
										 fprintf(fp,"%d_Weapons_%d\n", pity, primogems);
										//printf("%d_Weapons_%d\n", pity, primogems);


									}
								
									
								}
						}
						else{
							while((wait(&status7) > 0));
						}
					}
				}
			}
		}
	}
}
